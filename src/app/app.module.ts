import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { GraficasComponent } from './components/graficas/graficas.component';
import { RutasComponent } from './components/rutas/rutas.component';
import {RutasService} from './services/rutas/rutas.service'
import {EstadisticasService} from 'src/app/services/estadisticas/estadisticas.service'

// librerias
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    GraficasComponent,
    RutasComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    RutasService,
    EstadisticasService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
