import { Component, OnInit } from '@angular/core';
import {EstadisticasService} from 'src/app/services/estadisticas/estadisticas.service'
import { ConditionalExpr } from '@angular/compiler';

@Component({
  selector: 'app-graficas',
  templateUrl: './graficas.component.html',
  styleUrls: ['./graficas.component.css']
})
export class GraficasComponent implements OnInit {

  listDatos= {
    numeroBusDisponibles: "",
    numeroRutasDisponibles: ""
  }

  constructor(private EstadisticasService_:EstadisticasService) { }

  ngOnInit() {
    this.getEstadisticas();
  }

  getEstadisticas(){
    this.listDatos = {
      numeroBusDisponibles:"450",
      numeroRutasDisponibles: "150"
    }
    /*try{
      this.EstadisticasService_.getEstadisticas().subscribe(data=>{
        this.EstadisticasService_.getEstadisticas2().subscribe(data2=>{
          this.listDatos = {
            numeroBusDisponibles: data.count,
            numeroRutasDisponibles: data2.count
          }
        })
      })
    }catch(error){
      this.listDatos = {
        numeroBusDisponibles:"200",
        numeroRutasDisponibles: "150"
      }
    }*/
  }

}
