import { Component, OnInit } from '@angular/core';
import {RutasService} from '../../services/rutas/rutas.service';
import { interval } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-rutas',
  templateUrl: './rutas.component.html',
  styleUrls: ['./rutas.component.css']
})
export class RutasComponent implements OnInit {

  //Variables
  listRutas;

  constructor(private RutasService_:RutasService) { }

  ngOnInit() {
    this.listRutas = [];
    this.getRutas();
  }

  getRutas(){
    interval(10000).pipe(
      map((x) => { console.log("aaa") })
    ).subscribe(x => this.RutasService_.getRutas().subscribe(data=>{
      console.log("datos service",data);
      if(Array.isArray(data)){
        if(data.length > 0){
          data.forEach(element => this.listRutas.push(element));
          //this.listRutas.push(data);
        }
      }
    }));
    /*this.RutasService_.getRutas().subscribe(data=>{
      console.log("datos service",data);
      if(Array.isArray(data)){
        if(data.length > 0){
          data.forEach(element => this.listRutas.push(element));
          //this.listRutas.push(data);
        }
      }
    });*/
  }

}
