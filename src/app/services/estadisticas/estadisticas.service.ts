import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class EstadisticasService {

  constructor(private http: HttpClient) { }

  getEstadisticas(){
    let headers = new HttpHeaders({
      'Content-Type': 'application/json'
      // 'Authorization': `Bearer ${this.token}`
    });

    //LLAMADA SERVICE DE PRUEBAS!!
    return this.http.get('http://138.91.126.154/infraestructura/api/vehicle/TotalVehicleByZone', { headers });
  }

  getEstadisticas2(){
    let headers = new HttpHeaders({
      'Content-Type': 'application/json'
      // 'Authorization': `Bearer ${this.token}`
    });

    //LLAMADA SERVICE DE PRUEBAS!!
    return this.http.get('http://138.91.126.154/infraestructura/api/user/TotalUserByZone', { headers });
  }


}
