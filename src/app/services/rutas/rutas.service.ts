import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class RutasService {

  constructor(private http: HttpClient) { }

  getRutas(){
    let headers = new HttpHeaders({
      'Content-Type': 'application/json'
      // 'Authorization': `Bearer ${this.token}`
    });


    //LLAMADA SERVICE DE PRUEBAS!!
    return this.http.get('https://controltraficopoli.azurewebsites.net/api/Rutas', { headers });
  }


}
